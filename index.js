const functions = require("firebase-functions");
const admin = require("firebase-admin/app");
const Firestore = require("firebase-admin/firestore");
const Stripe = require("stripe");
const app = admin.initializeApp();
const firestore = Firestore.getFirestore(app);
const stripeWebhookSigningSecret = functions.config().stripe.stripe_webhook_signing_secret;
const stripeSecretKey = functions.config().stripe.stripe_secret_key;
const stripe = Stripe(stripeSecretKey)

const getPaymentMethod = async (paymentMethodId) => {
	try {
		return await stripe.paymentMethods.retrieve(paymentMethodId);
	} catch (err) {
		console.error('Error retrieving payment method: ', err);
		return null;
	}
};

const updateFirestore = (paymentIntent, event, payment_method) =>
	firestore.collection('payments').doc(paymentIntent.id).set({
		id: paymentIntent.id,
		amount: paymentIntent.amount,
		created: Firestore.Timestamp.fromMillis(paymentIntent.created * 1000),
		name: paymentIntent.metadata.name,
		place: paymentIntent.metadata.place,
		payment_method_types: paymentIntent.payment_method_types,
		customer: paymentIntent.customer,
		confirmation_method: paymentIntent.confirmation_method,
		statement_descriptor: paymentIntent.statement_descriptor,
		status: paymentIntent.status,
		client_secret: paymentIntent.client_secret,
		state_transitions: [{
			status: paymentIntent.status,
			time: Firestore.Timestamp.fromMillis(paymentIntent.created * 1000),
			type: event.type,
			event,
			payment_method
		}],
	})
	.then(() => console.log('Document successfully written!'))
	.catch((error) => console.error('Error writing document: ', error));

const handlePaymentIntentCreated = async (event) => {
	const paymentIntent = event.data.object;
	if (paymentIntent.metadata?.place) {
		const payment_method = paymentIntent.payment_method ? await getPaymentMethod(paymentIntent.payment_method) : null;
		await updateFirestore(paymentIntent, event, payment_method);
	} else {
		console.log("no metadata place key");
	}
};

const handlePaymentIntentStateTransition = async (event) => {
	const paymentIntent = event.data.object;
	const docRef = firestore.collection('payments').doc(paymentIntent.id);
	const payment_method = paymentIntent.payment_method ? await getPaymentMethod(paymentIntent.payment_method) : null;

	const currentDoc = await docRef.get();
	if (currentDoc.exists) {
		const currentData = currentDoc.data();
		const alreadyHandled = currentData.state_transitions.some(transition => transition.event_id === event.id);
		if (!alreadyHandled) {
			await docRef.update({
				status: paymentIntent.status,
				state_transitions: Firestore.FieldValue.arrayUnion({
					status: paymentIntent.status,
					time: Firestore.Timestamp.fromMillis(event.created * 1000),
					type: event.type,
					event,
					payment_method,
					event_id: event.id
				}),
			});
		}
	}
};

const handleEvents = {
	'payment_intent.created': handlePaymentIntentCreated,
	'payment_intent.processing': handlePaymentIntentStateTransition,
	'payment_intent.requires_action': handlePaymentIntentStateTransition,
	'payment_intent.succeeded': handlePaymentIntentStateTransition,
	'payment_intent.payment_failed': handlePaymentIntentStateTransition,
	'payment_intent.canceled': handlePaymentIntentStateTransition,
	'payment_intent.amount_capturable_updated': handlePaymentIntentStateTransition,
	'payment_intent.partially_funded': handlePaymentIntentStateTransition
};



exports.stripeWebhookHandler = functions.region('us-west2').https.onRequest(async (request, response) => {


	try {
		const event = stripe.webhooks.constructEvent(
			request.rawBody,
			request.headers["stripe-signature"],
			stripeWebhookSigningSecret
		);

		const handleEvent = handleEvents[event.type];
		if (handleEvent) {
			await handleEvent(event);
		} else {
			console.log(`Unhandled event type ${event.type}.`);
		}

		response.json({
			received: true
		});
	} catch (err) {
		console.error(`Webhook signature verification failed. `, err.message);
		response.status(400).send(`Webhook Error: ${err.message}`);
    }
});